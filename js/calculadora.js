(function ($) {
  $(document).ready(function () {


    // Búsqueda por cambio de entrada, meses.
    $('.slider-cal').each(function () {
      var $slider = $(this); // Ver atributos de la etiqueta del slider.

      //Nombre del campo entrada/meses
      var name = $slider.attr('name');

      // Valor mínimo del slider
      var min = 0;
      if ($slider.attr('min').trim().length) {
        min = parseInt($slider.attr('min'));
      }

      // Valor máximo del slider
      var max = 0;
      if ($slider.attr('max').trim().length) {
        max = parseInt($slider.attr('max'));
      }

      // Incremento del slider.
      var step = 1;
      if ($slider.is('[step]') && $slider.attr('step').trim().length) {
        step = parseInt($slider.attr('step'));
      }

      // Unidad de medida que se mestra en el slider.
      var unit = $slider.attr('unit');

      // Definir el inicio del Slider
      var variableOrden = $slider.parent().find('input[name="slider-orden"]').val();
      var start = min;
      if (variableOrden === 'DESC' && variableOrden.trim().length) {
        start = max;
      }

      // Llamada Slder JQuery UI.
      noUiSlider.create($slider[0], {
        start: [start],
        step: step,
        range: {
          'min': min,
          'max': max
        }
      });

      $slider[0].noUiSlider.on('update', function (values) {
        var $slider = $(this.target);
        var value = parseInt(values[0]);
        if (name === 'entrada') {
          var valuep = puntosNumero(value);
        }
        $('.slider-value[name="' + name + '"]').html(valuep + ' ' + unit);
        $('input[name="' + name + '"]').val(value);
      });

      $slider[0].noUiSlider.on('change', function (values) {
        actualizaPrecioFinanciacion();
      });
    });


    // Llama al AJAX para obtener el valor de financiación.
    var actualizaPrecioFinanciacion = function () {
      var parametros = {
        action: 'precio_financiacion_ajax',
        id: $('.calculadora').attr('data-id'),
        pv: $('input[name="precio-vehiculo"]').val(),
        en: $('input[name="entrada"]').val(),
        ms: $('input[name="meses"]').val()
      };

      $.ajax({
        url: form_busqueda_ajax.url,
        dataType: "json",
        type: 'get',
        data: parametros,
        success: function (response) {
          $('.precio-financiacion').html(response);
        }
      });
    }


  });


  var puntosNumero = function (value) {
    var retorno = '';
    value = value.toString().split('').reverse().join('');
    var i = value.length;
    while (i > 0) {
      retorno += ((i % 3 === 0 && i !== value.length) ? '.' : '') + value.substring(i--, i);
    }
    return retorno;
  };
})(jQuery);