<?php
/**
 * Created by IntelliJ IDEA.
 * User: Luis Calero, Juan Carlos Muruaga Ceballos.
 * Date: 27/09/2018
 * Time: 18:04
 */

class DataFinanciacion{
	private $mysqli;



	public function __construct($mysqli) {
		$this->mysqli = $mysqli;
	}



	/**
	 * Devuelve el total de financiación según:
	 * @param $idVehiculo number Identificador del vehículo.
	 * @param $entrada number Cantidad de la entrada;
	 * @param $meses number Cantidad de meses en pager.
	 *
	 * @return
	 * @throws Exception
	 */

  public function getPrecioFinanciacion($idVehiculo, $precio, $entrada, $mes ) {
    $sql =  " SELECT t" . $mes .
            " FROM " . DB_PREFIJO . "vehiculoFinanciacion" .
            " WHERE id_vehiculo=" . $idVehiculo;

	  $result = $this->mysqli->query($sql);
	  if (!$result) {
		  throw new Exception($this->mysqli->error);
	  }

	  $data =  mysqli_fetch_assoc( $result );
    $coheficiente = $data['t' . $mes];
    if (!$coheficiente) {
      throw new Exception($this->mysqli->error);
    }

	  $total = (intval($precio) - $entrada) * floatval($coheficiente);
	  $total = number_format((float)$total, 2, '.', '');

    return $total;
  }



	/**
	 * Devuelve el valor del precio del vehículo o de su oferta.
	 * @param $idVehiculo number Identificador del vehículo.
	 *
	 * @return number valor del precio o oferta.
	 * @throws Exception Excepción si no se encuentra el vehículo.
	 */

	public function getPrecioVehiculo($idVehiculo){

		$sql = ' SELECT pvpParticulares, pvpOfertaParticulares' .
		       ' FROM ' . DB_PREFIJO . 'vehiculoDatosEconomicos' .
		       ' WHERE id_vehiculo=' . $idVehiculo;

		$result = $this->mysqli->query($sql);
		if (!$result) {
			throw new Exception($this->mysqli->error);
		}

		$dataVehiculo = mysqli_fetch_assoc( $result );
		$precio = $dataVehiculo['pvpParticulares'];
		$precioOferta = $dataVehiculo['pvpOfertaParticulares'];

		if (empty($precioOferta) && $precioOferta > 0){
			$precio = $precioOferta;
		}

		return $precio;
	}



	public function getRangeMeses($idVehiculo) {

		$sql =  ' SELECT t12, t24, t36, t48, t60, t72, t84, t96, t108, t120' .
		        ' FROM ' . DB_PREFIJO . 'vehiculoFinanciacion' .
		        ' WHERE id_vehiculo=' . $idVehiculo;

		$arrayValores = ['120','108','96','84','72','60','48','36','24','12'];
		$arrayResultado = array();
		$arrayResultadoFinal = array();
		if ($result = $this->mysqli->query($sql)) {

      $data = mysqli_fetch_assoc($result);

		  if (!$data){
		    $arrayResultadoFinal = null;
      }
      else{
        $numrows = count($arrayValores);
        for($i = 0; $i < $numrows; $i++) {
          $valor = $data['t' . $arrayValores[$i]];
          if($valor != 0){
            array_unshift($arrayResultadoFinal, $arrayValores[$i]);
          }
        }
      }
		}

		return $arrayResultadoFinal;

	}

}