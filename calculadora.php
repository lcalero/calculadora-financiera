<?php
/**
 *
 * Plugin Name: cal-financiera
 * Plugin URL: no url
 * Description: Calculadora para la financiación
 * Version: 1.0.0
 * Author: Luis Calero
 *
 **/


define('PLUGIN_CALCULADORA_NAME', 'calculadora-financiera');


function calc_financiera ($atts){
	// Parámetros
  $class = ''; // Clase adicional al componente.
  $orden = '';
  extract( shortcode_atts( array(
    'class' => '',
    'orden' => 'ASC'
  ),$atts));

	if(isset($_GET['id'])) {
		$idVehiculo = $_GET['id'];
	}
	else if(isset($_POST['id'])) {
		$idVehiculo = $_POST['id'];
	}
	else {
		$query_string = $_SERVER['QUERY_STRING'];
		$arr = explode('-', $query_string);
		$count = sizeof($arr);
		$idVehiculo = $arr[$count - 1];
	}


  // Conexión BBDD.
  $mysqli = conectionBDatos();

	$financiacion = new DataFinanciacion($mysqli);

	$precio = $financiacion->getPrecioVehiculo($idVehiculo);
	$meses = $financiacion->getRangeMeses($idVehiculo);

  if (!empty($meses)) {
    $mesesEntrada = ($orden == 'DESC') ? end($meses) : $meses[0];
    $precioFinanciacion = $financiacion->getPrecioFinanciacion($idVehiculo, $precio,0, $mesesEntrada);


    if (!empty(TEMPLATE_CALCULADORA)) {
      include(get_stylesheet_directory() . '/' . PLUGIN_CALCULADORA_NAME . '/template-calculadora.php');
    } else {
      include 'html/template-parts/template-calculadora.php';
    }
  }else{
    include 'html/template-parts/template-noresultado.php';
  }

  // Fin conexión BBDD.
  closeBDatos($mysqli);
}


/**
 * CSS & JS.
 */
function calculadora_enqueue_scripts() {
	wp_enqueue_script( 'script-cal', plugins_url('/' . PLUGIN_CALCULADORA_NAME . '/js/calculadora.js'), array( 'jquery' ) );
  if (CSS_CALCULADORA) {
    wp_enqueue_style('style-cal', get_stylesheet_directory_uri() . '/'. PLUGIN_CALCULADORA_NAME . '/css/calculadora.css', __FILE__);
  }else{
    wp_enqueue_style( 'style-cal', plugins_url('/' . PLUGIN_CALCULADORA_NAME . '/css/calculadora.css'), __FILE__);
  }
	wp_localize_script('script-ajax', 'calculadora_ajax', ['url'=>admin_url('admin-ajax.php')]);
}
add_action( 'wp_enqueue_scripts', 'calculadora_enqueue_scripts' );



function requires() {
  require_once 'calculadora-common.php';
  require_once 'calculadora-config.php';
  require_once 'data/DataFinanciacion.php';

	require_once 'ajax/calculadora-ajax.php';
}
requires();


// Tag del plugin.
add_shortcode('calc_financiera', 'calc_financiera');