<?php



/**
 * Devuelve el precio de financiación por AJAX
 */
function ajaxPrecioFinanciacion(){
	$idVehiculo = strtolower($_GET['id']);
	$precioVehiculo = strtolower($_GET['pv']);
    $entrada = strtolower($_GET['en']);
	$mes = strtolower($_GET['ms']);


  // DB connection
  $mysqli = conectionBDatos();
  if ($mysqli->connect_error) {
	  die("Connection failed: " . $mysqli->connect_error);
  }
  $financiacion = new DataFinanciacion($mysqli);
  $precio =  $financiacion->getPrecioFinanciacion($idVehiculo, intval($precioVehiculo), intval($entrada), intval($mes));

  // DB close.
  closeBDatos($mysqli);

	echo $precio;

	wp_die();
}


add_action('wp_ajax_nopriv_precio_financiacion_ajax', 'ajaxPrecioFinanciacion' );
add_action('wp_ajax_precio_financiacion_ajax', 'ajaxPrecioFinanciacion' );