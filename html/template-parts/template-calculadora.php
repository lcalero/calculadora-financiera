<?php
/**
 * Created by IntelliJ IDEA.
 * User: Luis Calero, Juan Carlos Muruaga Ceballos.
 * Date: 04/10/2018
 * Time: 11:01
 */
?>
<script>
  (function ($) {
    $(document).ready(function () {
        var precio = $('.precio-financiacion').html();
        var mes = $('.slider-value[name="meses"]').html();
        $('#hide-finan').val(precio+' €/mes en '+ mes);
    });
  })(jQuery);
</script>
<div class="row calculadora <?php echo $class ?>" data-id="<?php echo $idVehiculo ?>"
     xmlns="http://www.w3.org/1999/html">
  <input type="hidden" name="precio-vehiculo" value="<?php echo $precio ?>">

  <h3 class="slider-title text-center">Cuota de entrada:</h3><span class="slider-value float-right" name="entrada"></span>
  <div class="contenedor-slider"><div class="form-control slider-cal" name="entrada" unit="€" min="0" max="<?php echo $precio ?>" step="1000"></div></div>
  <input type="hidden" name="entrada" value="0">

  <h3 class="slider-title text-center">Período de financiación:</h3><span class="slider-value float-right" name="meses"></span>
  <div class="contenedor-slider"><div class="form-control slider-cal" name="meses" unit="meses" min="<?php echo $meses[0] ?>" max="<?php echo end($meses) ?>" step="12"></div></div>
  <input type="hidden" name="meses" value="<?php echo end($meses) ?>">

  <div class="contenedor-financiacion">Financia tu coche por <strong><span class="precio-financiacion"><?php echo $precioFinanciacion ?></span> €/mes</strong> en <strong><span class="slider-value" name="meses"></span></strong></div>

  <div class="boton btn-finan">
    <a href="#popup3"><button type="button" class="vc_col-sm-12 vc_col-xs-12 btn-info qbutton medium center ">Quiero esta Financiación</button></a>
  </div>
  <div id="popup3" class="col-md-12">
    <div class="popup-contenedor">
      <div class="result-finan">
        <h4>Financiación:</h4>
        <strong><span class="precio-financiacion"><?php echo $precioFinanciacion ?></span> €/mes</strong> en <strong><span class="slider-value" name="meses"></span></strong>
      </div>
      <?php echo do_shortcode(CONTACTO_FINANCIACION) ?>
      <a class="popup-cerrar" href="#close">X</a>
    </div>
  </div>
</div>