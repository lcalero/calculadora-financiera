<?php
/**
 * Created by IntelliJ IDEA.
 * User: Luis
 * Date: 02/10/2018
 * Time: 12:04
 */

function conectionBDatos(){
  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  $mysqli->set_charset('utf8');
  if($mysqli->connect_error){
    die("Connection failed: " . $mysqli->connect_error);
  }
  return $mysqli;
}

function closeBDatos($mysqli) {
  $mysqli->close();
}
